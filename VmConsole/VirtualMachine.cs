﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmConsole
{
    public class VirtualMachine
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ParentNode { get; set; }
    }
}
