﻿using Corsinvest.ProxmoxVE.Api;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Corsinvest.ProxmoxVE.Api.PveClient.PVENodes.PVEItemNode;

namespace VmConsole
{
    public partial class LoginForm : Form
    {
        private List<VirtualMachine> discoveredVms = new List<VirtualMachine>();
        private PveClient client;

        public LoginForm()
        {
            InitializeComponent();
            // Check if data is saved in registry
            var key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\VmConsole");
            if (key == null) key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\VmConsole");

            var savedIp = key.GetValue("savedIp");
            var savedUser = key.GetValue("savedUser");
            var savedPass = key.GetValue("savedPassword");
            if (savedIp != null) ipTextbox.Text = savedIp as string;
            if (savedUser != null) usernameTextbox.Text = savedUser as string;
            if (savedPass != null) passwordTextbox.Text = savedPass as string;
            key.Close();
            //ipTextbox.KeyUp += submitForm;
            //usernameTextbox.KeyUp += submitForm;
            //passwordTextbox.KeyUp += submitForm;
            AcceptButton = loginButton;
        }

        private void submitForm(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                loginButton_click(sender, e);
                e.Handled = true;
            }
        }

        private void loginButton_click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ipTextbox.Text))
            {
                MessageBox.Show("You must enter an IP address.");
                return;
            }
            if (string.IsNullOrEmpty(usernameTextbox.Text))
            {
                MessageBox.Show("You must enter a username");
                return;
            }
            if (string.IsNullOrEmpty(passwordTextbox.Text))
            {
                MessageBox.Show("You must enter a password");
                return;
            }

            client = new PveClient(ipTextbox.Text);
            var username = usernameTextbox.Text;
            if (!username.Contains("@")) username = username += "@pve";
            if (client.Login(username, passwordTextbox.Text))
            {
                // save login
                var key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\VmConsole", RegistryKeyPermissionCheck.ReadWriteSubTree);
                key.SetValue("savedIp", ipTextbox.Text);
                key.SetValue("savedUser", usernameTextbox.Text);
                key.SetValue("savedPassword", passwordTextbox.Text);

                discoveredVms.Clear();
                //MessageBox.Show(client.Nodes.GetRest().Response.data.ToString());
                foreach (var node in client.Nodes.GetRest().Response.data)
                {
                    var data = client.Nodes[node.node].Qemu.GetRest().Response.data as List<object>;
                    if (data != null)
                        foreach (var vm in data)
                        {
                            var qemuVm = (dynamic)vm;
                            discoveredVms.Add(new VirtualMachine()
                            {
                                Id = int.Parse(qemuVm.vmid),
                                Name = qemuVm.name,
                                ParentNode = node.node
                            });
                        }
                }
                vmListBox.Visible = true;
                connectButton.Visible = true;
                AcceptButton = connectButton;
                vmListBox.Items.Clear();
                foreach (var vm in discoveredVms)
                {
                    vmListBox.Items.Add(vm.Name);
                }
                
            }
            else
            {
                MessageBox.Show("Could not log into Proxmox - is your username and password correct?");
                return;
            }
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            var vm = discoveredVms[vmListBox.SelectedIndex];
            var spiceproxy = client.Nodes[vm.ParentNode].Qemu[vm.Id].Spiceproxy.Spiceproxy();

            var spiceProxyConfig = spiceproxy.Response.data as IDictionary<string, object>;
            //spiceProxyConfig.Remove("proxy");
            //spiceProxyConfig.Add("proxy", "http://" + ipTextbox.Text + ":3128");
            var uri = new Uri(spiceProxyConfig["proxy"] as string);
            spiceProxyConfig.Remove("proxy");
            spiceProxyConfig.Add("proxy", uri.Scheme + "://" + ipTextbox.Text + ":" + uri.Port);

            var virtViewerConfig = "[virt-viewer]\n";
            foreach (var kvPair in spiceProxyConfig)
            {
                virtViewerConfig += kvPair.Key + "=" + kvPair.Value + "\n";
            }
            var fn = Path.GetTempFileName() + ".vv";
            File.WriteAllText(fn, virtViewerConfig);
            Process fileopener = new Process();
            fileopener.StartInfo.FileName = "explorer";
            fileopener.StartInfo.Arguments = fn;
            fileopener.Start();
            Application.Exit();
        }
    }
}
